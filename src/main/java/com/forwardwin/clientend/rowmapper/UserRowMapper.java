package com.forwardwin.clientend.rowmapper;

import com.forwardwin.clientend.entities.UserBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sami on 15/8/14.
 */
public class UserRowMapper implements RowMapper<UserBean> {

    //long orderId, long userId, int likeNum, int likeGot, int followNum, int followGot, int showNum, String state, int amount, String postId, String targetUserId, String targetUserAvatar, String postUrl, String postUrlLow, String thumbnailUrl, Date createTime, Date finishTime, int fromBot) {

    @Override
    public UserBean mapRow(ResultSet resultSet, int i) throws SQLException {
        UserBean user = new UserBean(
                resultSet.getLong("userid"),
                resultSet.getString("username"),
                resultSet.getString("password"),
                resultSet.getInt("like_count"),
                resultSet.getInt("follow_count"),
                resultSet.getInt("isactive")
        );

        return user;

    }
}
