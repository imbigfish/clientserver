package com.forwardwin.clientend.daos;

import com.forwardwin.clientend.dtos.OrderStatistics;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class OrderStatisticsDao {

	public void setOrderStat(Map<Long, OrderStatistics> orderStat) {

		this.orderStat = orderStat;
	}

	public Map<Long, OrderStatistics> getOrderStat() {

		return orderStat;
	}

	private Map<Long, OrderStatistics> orderStat = new ConcurrentHashMap<Long, OrderStatistics>();

	private static Logger logger = Logger.getLogger(OrderStatisticsDao.class);
	
	public OrderStatistics getOrderStatByOrderId(Long orderId) {
		return orderStat.get(orderId);
	}

}
