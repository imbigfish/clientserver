package com.forwardwin.clientend.daos;

import com.forwardwin.clientend.Util.Constant;
import com.forwardwin.clientend.Util.Util;
import com.forwardwin.clientend.entities.CLoginRequestParams;
import com.forwardwin.clientend.entities.CTrackActionParams;
import com.forwardwin.clientend.entities.UserBean;
import com.forwardwin.clientend.rowmapper.UserRowMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;


@Component
public class UserDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static Logger logger = Logger.getLogger(UserDao.class);

    public boolean saveOrUpdateUser(CLoginRequestParams login) {

        try {
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            int existCount = jdbcTemplate.update("update cbot set login_time = ? where userid = ? limit 1", new Object[]{currentTimestamp, login.getUserId()});
            if (existCount == 0) {
                jdbcTemplate.update("insert into cbot (username, password, userid, login_time, isactive) values (?, ?, ?, ?, ?)",
                        new Object[]{
                                login.getUsername(), login.getDescription(), login.getUserId(), currentTimestamp, login.getIsactive()
                        });
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //get user table index userId and check user permission
    public UserBean checkUserPermissionAndGetUser(long userId) {
        try {
            UserBean user = jdbcTemplate.queryForObject("SELECT * FROM cbot WHERE userid = ? LIMIT 1", new Object[]{userId}, new UserRowMapper());
            return user;
        } catch (Exception e) {
            logger.info("Cannot find user checkUserPermissionAndGetUser:" + userId);
//            e.printStackTrace();
        }
        return null;
    }

    public boolean trackAction(UserBean user, CTrackActionParams trackction) {

        try {
            int existCount = 0;
            if (trackction.getOrderKind() == 2) {
                int likeCount = user.getLikeCount() + 1;
                existCount = jdbcTemplate.update("update cbot set like_count = ? where userid = ? limit 1", new Object[]{likeCount, trackction.getUserId()});
            } else if (trackction.getOrderKind() == 3) {
                int followCount = user.getFollowCount() + 1;
                existCount = jdbcTemplate.update("update cbot set follow_count = ? where userid = ? limit 1", new Object[]{followCount, trackction.getUserId()});
            }

            return existCount > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public JdbcTemplate getJdbc() {
        return this.jdbcTemplate;
    }

}