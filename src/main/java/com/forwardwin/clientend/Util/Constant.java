package com.forwardwin.clientend.Util;

/**
 * Created by sami on 15/8/21.
 */
public class Constant {
    //config
    public static String LIKE_REWARD = "likeReward";
    public static String REVINE_REWARD = "revineReward";
    public static String FOLLOW_REWARD = "followReward";

    public static String DEFAULT_BORAD_ITEM_NUMBER = "defaultBoardItemNumber";
    public static String DEFAULT_MIX_LIKE_ITEM_NUMBER = "defaultMixLikeItemNumber";
    public static String DEFAULT_MIX_FOLLOW_ITEM_NUMBER = "defaultMixFollowNumber";

    public static String COLLAGE_SIZE = "collageSize";

    public static String enableBotOrderRearrance = "enableBotOrderRearrance";
    public static String llimit = "llimit";
    public static String flimit = "flimit";

    public static String actionCoolDown = "actionCoolDown";
    public static String videoCoins = "videoCoins";
    public static String rateString = "rateString";
    public static String iosPackageName = "iosPackageName";
    public static String androidPackageName = "androidPackageName";
    public static String inviteReward = "inviteReward";
    public static String inrVersion = "inrVersion";
    public static String defaultFreeCoin = "defaultFreeCoin";
    public static String ssEnabled = "ssEnabled";
    public static String nativexEnabled = "nativexEnabled";
    public static String tapjoyEnabled = "tapjoyEnabled";
    public static String admobEnabled = "admobEnabled";
    public static String inviteString = "inviteString";
    public static String faqUrl = "faqUrl";
    public static String showNumRatio = "showNumRatio";
    public static String getLoopsEnabled = "getLoopsEnabled";
    public static String getRevineEnabled = "getRevineEnabled";
    public static String signatureEnabled = "signatureEnabled";
    public static String loadSeenOrder = "loadSeenOrder";
    public static String inviteEnabled = "inviteEnabled";
    public static String promoteAppDescription = "promoteAppDescription";
    public static String androidPublicKey = "androidPublicKey";
    public static String beInvitedReward = "beInvitedReward";
    public static String superRateReward = "superRateReward";
    public static String inviteContent = "inviteContent";
    public static String inviteTitle = "inviteTitle";
    public static String hideGetLikes = "hideGetLikes";
    public static String hideGetFollower = "hideGetFollower";
    public static String forbidRate = "forbidRate";
    public static String transfer = "transfer";
    public static String realCloseRatio = "realCloseRatio";
    public static String checkGetBoard = "checkGetBoard";
    public static String resetEmptyGetBoardUserListTimer = "resetEmptyGetBoardUserListTimer";
    public static String getBoardSelectState = "getBoardSelectState";
    public static String userSeenDays = "userSeenDays";


    //constant state
    public static int USER_STATE_NORMAL = 1;
    public static int USER_STATE_BAN = 9;



    public static int ORDER_STATE_NOT_PAID = 0;
    public static int ORDER_STATE_PAID = 1;
    public static int ORDER_STATE_PROMOTE = 2;
    public static int ORDER_STATE_PROMOTE_FINISH = 3;
    public static int ORDER_STATE_BOT_ACTION = 4;
    public static int ORDER_STATE_CLOSE = 5;
    public static int ORDER_STATE_FAIL = 6;
    public static int ORDER_STATE_SPARE = 7;

    public static String COIN_CHANGE_REASON_BUY_LIKE = "BUY_LIKE";
    public static String COIN_CHANGE_REASON_BUY_REVINE = "BUY_REVINE";
    public static String COIN_CHANGE_REASON_BUY_LOOP = "BUY_LOOP";
    public static String COIN_CHANGE_REASON_BUY_FOLLOW = "BUY_FOLLOW";
    public static String COIN_CHANGE_REASON_LIKE_REWARD = "LIKE_REWARD";
    public static String COIN_CHANGE_REASON_REVINE_REWARD = "REVINE_REWARD";
    public static String COIN_CHANGE_REASON_FOLLOW_REWARD = "FOLLOW_REWARD";
    public static String COIN_CHANGE_REASON_BUY_COIN = "BUY_COIN";
    public static String COIN_CHANGE_REASON_BUY_REMOVEAD = "REMOVE_AD";
    public static String COIN_CHANGE_REASON_NORMALUSE = "DOWNLOAD_OR_REPOST";

    public static int ACTION_TRACK_CODE_DO = 0;
    public static int ACTION_TRACK_CODE_SKIP = 1;
    public static int ACTION_TRACK_CODE_DISPATCH = 2;


    public static int BOARD_TYPE_LIKE = 0;//"like";
    public static int BOARD_TYPE_FOLLOW = 1;//"follow";
    public static int BOARD_TYPE_MIX = 2;//"mix";
    public static int BOARD_TYPE_REVINE = 3;//"revine";

    public static String ORDER_TYPE_LIKE = "like";
    public static String ORDER_TYPE_REVINE = "revine";
    public static String ORDER_TYPE_LOOPS = "loops";
    public static String ORDER_TYPE_FOLLOW = "follow";
    public static String ORDER_TYPE_COLLAGE = "collage";

    public static int QUERY_ORDER_UNFINISHED = 0;
    public static int QUERY_ORDER_FINISHED = 1;



    //payment
    public static int GOODS_TYPE_LIKE = 2;
    public static int GOODS_TYPE_FOLLOW = 3;
    public static int GOODS_TYPE_COIN = 0;
    public static int GOODS_TYPE_REVINE = 4;
    public static int GOODS_TYPE_LOOPS = 5;
    public static int GOODS_TYPE_STORE_SPECIAL = 10;

    //offerwall platform
    public static String PLATFORM_SUPERSONIC = "supersonic";
    public static String PLATFORM_TAPJOY = "tapjoy";
    public static String PLATFORM_NATIVEX = "nativex";

    //appName
    public static String APPNAME_DEFAULT = "default";

    //orderKind
    public static final int ORDERKIND_LIKE = 2;
    public static final int ORDERKIND_FOLLOW = 3;
    public static final int ORDERKIND_REVINE = 4;
    public static final int ORDERKIND_LOOP = 5;

    public static int MIN_FINISH_COUNT_LOOP = 0;
    public static int MAX_FINISH_COUNT_LOOP = 10;

    public static final String AMAZON_DEVELOP_KEY = "2:HgualAJjaaeS5QOHf7g00NBwjRQ0p5TtKUQNsiGUv3Bl5ibah-yF0RT9ZhhnW5f0:d_Mi0OfWKm4jpdheFB7GZQ==";
    public static final String AMAZON_VERIFICATION_URL = "https://appstore-sdk.amazon.com/version/1.0/verifyReceiptId/developer/" + AMAZON_DEVELOP_KEY + "/user/%s/receiptId/%s";

    //tasktype
    public static int TaskType_RateUs = 0;
    public static int TaskType_DailyLogin = 1;
    public static int TaskType_ShareFacebook = 2;
    public static int TaskType_ShareTwitter = 3;
    public static int TaskType_ShoutOut = 4;
    public static int TaskType_FollowUs = 5;
    public static int TaskType_Download = 6;
    public static int TaskType_Invite = 7;
}
