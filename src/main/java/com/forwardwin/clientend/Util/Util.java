package com.forwardwin.clientend.Util;


import org.apache.http.util.TextUtils;
import sun.misc.BASE64Decoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by sami on 15/8/1.
 */
public class Util {

    public static int random(int m,int n){
        Random rand = new Random();
        return rand.nextInt(n-m)+m;
    }

    public static String clearEmailChars(String str){
        String r = str.toLowerCase();
        String res = "";
        for (int i = 0; i < r.length(); i++) {
            if((r.charAt(i)>='a'&&r.charAt(i)<='z')||(r.charAt(i)>='0'&&r.charAt(i)<='9')){
                res = res+String.valueOf(r.charAt(i));
            }
        }

        return res;
    }

    public static long getUnixTimestamp(){
        long timestamp = new Date().getTime()/1000;
        return timestamp;
    }

    @SuppressWarnings("deprecation")
    public static String toDateStr(long t){
        Date d = new Date(t);
        return d.getYear()+"-"+d.getMonth()+"-"+d.getDay()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    }

    public static String md5(String s) {
        char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
        try {
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            byte[] md = mdInst.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

	// return style of 1,3,4
    public static String setToString(Set<Long> target)
    {
		StringBuilder sb = new StringBuilder();
		sb.append('(');
		
		if(target != null && target.size() != 0)
		{
			boolean first = true;
			
			for(Long orderId : target)
			{
				if(first == false)
				{
					sb.append(',');
				}
				else
				{
					first = false;
				}
				sb.append(orderId);
			}
		}
		sb.append(')');
		
		return sb.toString();
    }

    public static String listToString(List<Long> target)
    {
        StringBuilder sb = new StringBuilder();
        sb.append('(');

        if(target != null && target.size() != 0)
        {
            boolean first = true;

            for(Long orderId : target)
            {
                if(first == false)
                {
                    sb.append(',');
                }
                else
                {
                    first = false;
                }
                sb.append(orderId);
            }
        }
        sb.append(')');

        return sb.toString();
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String getFromBase64(String s) {
        byte[] b = null;
        String result = null;
        if (s != null) {
            BASE64Decoder decoder = new BASE64Decoder();
            try {
                b = decoder.decodeBuffer(s);
                result = new String(b, "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static boolean checkSignatureFailed(String jsonbody, String signature) {

        if (signature.equalsIgnoreCase("whosyourdaddy")){
            return false;
        }
        if (TextUtils.isEmpty(signature)){
            return true;
        }
        if (!Security.isHMACSigatureValid(jsonbody, signature)) {
            return true;
        }
        return false;
    }

    public static long getCurrentTimeStamp() {

        return System.currentTimeMillis() / 1000;
    }
}
