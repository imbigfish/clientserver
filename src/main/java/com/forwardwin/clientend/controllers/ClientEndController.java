package com.forwardwin.clientend.controllers;

import com.forwardwin.clientend.Util.JsonHelper;
import com.forwardwin.clientend.Util.Util;
import com.forwardwin.clientend.daos.OrderStatisticsDao;
import com.forwardwin.clientend.daos.UserDao;
import com.forwardwin.clientend.entities.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * Created by forwardwin on 7/25/16.
 */

@RestController
@RequestMapping("/in")
public class ClientEndController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private OrderStatisticsDao orderStatisticsDao;

    @RequestMapping(value = "/helloworld", method = RequestMethod.GET)
    public String helloworld(){
        return "Hello World!";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResultObject login(@RequestBody CLoginRequestParams request){

        if (userDao.saveOrUpdateUser(request)) {
            return new ResultObject(StatusResult.SUCCESS);
        } else {
            return new ResultObject(StatusResult.FAIL);
        }
    }

    @RequestMapping(value = "/updateOrders", method = RequestMethod.GET)
    public ResultObject updateOrders() throws IOException {

        String url = "http://instalike.socialmarkets.info/bot/orderState/useronlyorder";
        HttpClient httpclient = new DefaultHttpClient();

        HttpGet httpgets = new HttpGet(url);
        httpgets.setHeader("Accept-Encoding", "gzip, deflate");
        httpgets.setHeader("Accept", "*/*");

        HttpResponse response = httpclient.execute(httpgets);
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            GZIPInputStream instreams = new GZIPInputStream(entity.getContent());
            String data = Util.convertStreamToString(instreams);
            GetFriendShipResponse getFriendShipResponse = JsonHelper.gson.fromJson(data, GetFriendShipResponse.class);
            orderStatisticsDao.setOrderStat(getFriendShipResponse.getData());
            httpgets.abort();
        }

        return new ResultObject(StatusResult.SUCCESS);
    }

    @RequestMapping(value = "/friendship", method = RequestMethod.POST)
    public ResultObject cGetBoard(@RequestBody String jsonbody,
                                  @RequestHeader(value = "Signature", required = false) String signature) {
        if (Util.checkSignatureFailed(jsonbody, signature)) {
            return new ResultObject(StatusResult.FAIL);
        }

        long currentTimeStamp = Util.getCurrentTimeStamp();
        CGetBoardParams request = JsonHelper.gson.fromJson(jsonbody, CGetBoardParams.class);
        if (userDao.checkUserPermissionAndGetUser(request.getUserId()) == null) {
            return new ResultObject(StatusResult.USER_NOT_FOUND);
        } else if (currentTimeStamp < request.getTimeStamp() && (request.getTimeStamp() + 600) >= currentTimeStamp) {
            return new ResultObject(StatusResult.FAIL);
        }

        return new ResultObject(StatusResult.SUCCESS, orderStatisticsDao.getOrderStat());
    }

    @RequestMapping(value = "/trackaction", method = RequestMethod.POST)
    public ResultObject cTrackAction(@RequestBody String jsonbody,
                                     @RequestHeader(value = "Signature", required = false) String signature) throws IOException {
        if (Util.checkSignatureFailed(jsonbody, signature)) {
            return new ResultObject(StatusResult.FAIL);
        }

        long currentTimeStamp = Util.getCurrentTimeStamp();
        CTrackActionParams request = JsonHelper.gson.fromJson(jsonbody, CTrackActionParams.class);
        UserBean user = userDao.checkUserPermissionAndGetUser(request.getUserId());
        if (user == null) {
            return new ResultObject(StatusResult.USER_NOT_FOUND);
        } else if (currentTimeStamp < request.getTimeStamp() && (request.getTimeStamp() + 600) >= currentTimeStamp) {
            return new ResultObject(StatusResult.FAIL);
        }

        if (userDao.trackAction(user, request)) {
            String url = String.format("http://instalike.socialmarkets.info/bot/IAmBot/traceAction/BotPassword/%d", request.getOrderId());
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpgets = new HttpGet(url);
            httpgets.setHeader("Accept-Encoding", "gzip, deflate");
            httpgets.setHeader("Accept", "*/*");
            httpclient.execute(httpgets);

            return new ResultObject(StatusResult.SUCCESS);
        }

        return new ResultObject(StatusResult.FAIL);
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.GET)
    public ResultObject getOrders(){

        return new ResultObject(StatusResult.SUCCESS);
//        return new ResultObject(StatusResult.SUCCESS, orderStatisticsDao.getOrderStat());
    }
}
