package com.forwardwin.clientend.dtos;


public class OrderStatistics {

	private long orderId;
	private long userId;
	private int likeNum;
	private int likeGot;
	private int orderKind;
	private int followNum;
	private int followGot;
	private int showNum;
	private int state;
	private long targetUserId;
	private long postId;

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getLikeNum() {
		return likeNum;
	}

	public void setLikeNum(int likeNum) {
		this.likeNum = likeNum;
	}

	public int getLikeGot() {
		return likeGot;
	}

	public void setLikeGot(int likeGot) {
		this.likeGot = likeGot;
	}

	public int getOrderKind() {
		return orderKind;
	}

	public void setOrderKind(int orderKind) {
		this.orderKind = orderKind;
	}

	public int getFollowNum() {
		return followNum;
	}

	public void setFollowNum(int followNum) {
		this.followNum = followNum;
	}

	public int getFollowGot() {
		return followGot;
	}

	public void setFollowGot(int followGot) {
		this.followGot = followGot;
	}

	public int getShowNum() {
		return showNum;
	}

	public void setShowNum(int showNum) {
		this.showNum = showNum;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public long getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(long targetUserId) {
		this.targetUserId = targetUserId;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}
}
