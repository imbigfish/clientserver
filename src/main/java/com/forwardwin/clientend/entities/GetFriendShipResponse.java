package com.forwardwin.clientend.entities;

import com.forwardwin.clientend.dtos.OrderStatistics;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by forwardwin on 2/12/16.
 */
public class GetFriendShipResponse extends BasicResponse {

    private Map<Long, OrderStatistics> data;

    public Map<Long, OrderStatistics> getData() {
        return data;
    }

    public void setData(Map<Long, OrderStatistics> data) {
        this.data = data;
    }
}
