package com.forwardwin.clientend.entities;

public class StatusResult {
	private Integer status = 200;
	private String statusTag = "";
	private String statusMsg = "Success";

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer resultCode) {
		this.status = resultCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String resultDesc) {
		this.statusMsg = resultDesc;
	}
	public String getStatusTag() {
		return statusTag;
	}
	public void setStatusTag(String statusTag) {
		this.statusTag = statusTag;
	}
	
	public StatusResult(Integer resultCode, String statusTag, String resultDesc)
	{
		this.status = resultCode;
		this.statusTag = statusTag;
		this.statusMsg = resultDesc;
	}
	public StatusResult()
	{}
	
	public static StatusResult SUCCESS = new StatusResult(200, "Success", "Success");
	public static StatusResult ORDER_PAID = 				new StatusResult(200, "Order", 		"Good Order");


	public static StatusResult INVALID_TOKEN = 				new StatusResult(400, "Token", 	"Invalid Session Token");
	public static StatusResult ALREADY_PERFORMED = 			new StatusResult(404, "Action", "Action already Performed");
	public static StatusResult ALREADY_REWARED = 			new StatusResult(404, "Action", "Action already Rewared");
	public static StatusResult ORDER_NOT_ENOUGH = 			new StatusResult(404, "Order", 	"Not enough order found");
	public static StatusResult ORDER_NOT_FOUND = 			new StatusResult(404, "Order", 	"Cannot find order for the user");
	public static StatusResult USER_NOT_FOUND =				new StatusResult(404, "User", 	"Cannot find user");
	public static StatusResult GOODS_NOT_EXIST =			new StatusResult(404, "Goods", 	"Goods not exist");
	public static StatusResult TRACK_ORDER_FAILED = 		new StatusResult(404, "User", 	"Cannot track order");
	public static StatusResult UNKNOWN_REWARD = 			new StatusResult(405, "Reward", "Unknown reward type");
	public static StatusResult DUPLICATE_DAILY_LOGIN = 		new StatusResult(406, "Reward", "Duplicate daily checkin");
	public static StatusResult DUPLICATE_COMMENT_VERSION = 	new StatusResult(407, "Reward", "Duplicate comment on this version");
	public static StatusResult DUPLICATE_SHARE_FACEBOOK = 	new StatusResult(408, "Reward", "Duplicate reward on share to FaceBook");
	public static StatusResult DUPLICATE_SHARE_TWITTER = 	new StatusResult(409, "Reward", "Duplicate reward on share to Twitter");
	public static StatusResult DUPLICATE_DEVICE_DONWLOAD = 	new StatusResult(409, "Reward", "Device has downloaded this app before");
	public static StatusResult DUPLICATE_REWARD = 	new StatusResult(409, "Reward", "Duplicate reward on task");

	public static StatusResult NOT_ENOUGH_COIN = 			new StatusResult(410, "Coin", 	"Not Enought Coin");

	public static StatusResult UPDATE_VERSION = 			new StatusResult(420, "Update", "Please download the newest version");


	public static StatusResult FAIL = 						new StatusResult(500, "Fail", 	"Fail");
	public static StatusResult ORDER_GOOGLE_FAILED = 		new StatusResult(501, "Order",	"Can't access google");
	public static StatusResult ORDER_APPLE_FAILED = 		new StatusResult(501, "Order",	"Can't receive Apple response");
	public static StatusResult ORDER_NOT_PAID = 			new StatusResult(502, "Order", 	"Order not paied");
	public static StatusResult ORDER_INVALID = 				new StatusResult(503, "Order", 	"Order Invalid");
	public static StatusResult ORDER_ALREADY_USERD = 		new StatusResult(504, "Order", 	"Order Already validated");
	public static StatusResult REFER_CODE_INVALID = 		new StatusResult(505, "Refer", 	"Referal code not exist");
	public static StatusResult USER_CANNOT_INVITE_SELF = 	new StatusResult(505, "Refer", 	"You cannot invite yourself");
	public static StatusResult USER_ALREADY_INVITED = 		new StatusResult(506, "Refer", 	"The Device or User has been used for invitation");
	public static StatusResult ORDER_KIND_MISMATCH = 		new StatusResult(507, "Order", 	"Order Kind Mismatch");
	public static StatusResult ORDER_FINISH_COUNT_INVALID = new StatusResult(508, "Order", 	"Order Finish Count Invalid");

	public static StatusResult DATABASE_ERROR = 			new StatusResult(700, "Database", "Database Data Error");


	public static StatusResult INVALID_COIN = 				new StatusResult(800, "Coins", 	"Invalid Parameter Coins");

}
