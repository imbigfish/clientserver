package com.forwardwin.clientend.entities;

public class FriendShip {

    private long orderId;
    private long userId;
    private long likeNum;
    private long likeGot;
    private long orderKind;
    private long followNum;
    private long followGot;

    private long state;
    private long postId;
    private long targetUserId;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(long likeNum) {
        this.likeNum = likeNum;
    }

    public long getLikeGot() {
        return likeGot;
    }

    public void setLikeGot(long likeGot) {
        this.likeGot = likeGot;
    }

    public long getOrderKind() {
        return orderKind;
    }

    public void setOrderKind(long orderKind) {
        this.orderKind = orderKind;
    }

    public long getFollowNum() {
        return followNum;
    }

    public void setFollowNum(long followNum) {
        this.followNum = followNum;
    }

    public long getFollowGot() {
        return followGot;
    }

    public void setFollowGot(long followGot) {
        this.followGot = followGot;
    }

    public long getState() {
        return state;
    }

    public void setState(long state) {
        this.state = state;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(long targetUserId) {
        this.targetUserId = targetUserId;
    }
}
