package com.forwardwin.clientend.entities;

/**
 * Created by sami on 15/8/14.
 */

/*
* CREATE TABLE `user` (
  `userId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `platform` tinyint(11) DEFAULT '0' COMMENT '1:android 2:iphone 3:ipad 4:other',
  `imei` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `deviceId` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `coin` int(11) DEFAULT '0' COMMENT '用户账户余额',
  `state` tinyint(4) DEFAULT '1' COMMENT '1:正常 2: 禁止 3:永久禁止',
  `lastLoginIp` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastLoginTime` timestamp NULL DEFAULT NULL,
  `registerTime` int(10) DEFAULT NULL,
  `rank` tinyint(4) DEFAULT '0' COMMENT '0:普通 1:VIP 2: 高级VIP',
  `sessionToken` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'vine api key',
  `vineUserId` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'vine 平台userId',
  `vineUsername` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `vinePassword` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2147483650 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
*
* */
public class UserBean {
    private String username;
    private String password;
    private long userId;
    private int likeCount;
    private int followCount;
    private int isactive;

    public UserBean() {
    }

    public UserBean(long userId, String username, String password, int likeCount, int followCount, int isactive) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.likeCount = likeCount;
        this.followCount = followCount;
        this.isactive = isactive;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getFollowCount() {
        return followCount;
    }

    public void setFollowCount(int followCount) {
        this.followCount = followCount;
    }

    public int getIsactive() {
        return isactive;
    }

    public void setIsactive(int isactive) {
        this.isactive = isactive;
    }
}
