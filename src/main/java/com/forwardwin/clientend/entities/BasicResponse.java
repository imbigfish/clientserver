package com.forwardwin.clientend.entities;

public class BasicResponse {

  protected RequestStatus status;

  public RequestStatus getStatus() {

      return status;
  }

  public void setStatus(RequestStatus status) {

      this.status = status;
  }

  public boolean isSuccessful() {

      return status != null && status.getStatus() == 200;
  }

  public boolean isSessionExpired() {

      return status != null && status.getStatus() == 400;
  }

  public boolean needUpdateVersion() {

      return status != null && status.getStatus() == 420;
  }
}
