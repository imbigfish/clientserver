package com.forwardwin.clientend.entities;

public class ResultObject {

	private StatusResult status;
	private Object data;

	public StatusResult getStatus() {
		return status;
	}

	public void setStatus(StatusResult status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public ResultObject(StatusResult status, Object data)
	{
		this.status = status;
		this.data = data;
	}
	
	public ResultObject(StatusResult status)
	{
		this.status = status;
	}
}
