package com.forwardwin.clientend.entities;

/**
 * Created by forwardwin on 7/26/16.
 */
public class CGetBoardParams {

    private int userId;
    private long timeStamp;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
