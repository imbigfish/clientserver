#!/bin/bash

#echo Pull code from "$1"

echo git reset all local changes
git reset --hard


#    git fetch && git checkout master
#    git pull


echo pull code
git pull

echo Code pull complete

echo record current commit
echo "|=========================================================|" >> lastcommit.txt
date >> lastcommit.txt
echo "|-------------------------------|" >> lastcommit.txt
git log -1>>lastcommit.txt
echo "|-------------------------------|" >> lastcommit.txt
git status>>lastcommit.txt
echo "|_________________________________________________________|" >> lastcommit.txt


echo start mvn install
mvn install

echo start server clientserver
sudo /usr/local/bin/supervisorctl restart clientserver

echo start tailing supervisor_clientserver_stdout.log
tail -n 500 -f /home/ec2-user/Code/clientserver/clientserver/target/supervisor_clientserver_stdout.log

echo "end here $target"
